let people = [
    {
        id: 1,
        first_name: "Valera",
        last_name: "Pinsent",
        email: "vpinsent0@google.co.jp",
        gender: "Male",
        ip_address: "253.171.63.171",
    },
    {
        id: 2,
        first_name: "Kenneth",
        last_name: "Hinemoor",
        email: "khinemoor1@yellowbook.com",
        gender: "Polygender",
        ip_address: "50.231.58.150",
    },
    {
        id: 3,
        first_name: "Roman",
        last_name: "Sedcole",
        email: "rsedcole2@addtoany.com",
        gender: "Genderqueer",
        ip_address: "236.52.184.83",
    },
    {
        id: 4,
        first_name: "Lind",
        last_name: "Ladyman",
        email: "lladyman3@wordpress.org",
        gender: "Male",
        ip_address: "118.12.213.144",
    },
    {
        id: 5,
        first_name: "Jocelyne",
        last_name: "Casse",
        email: "jcasse4@ehow.com",
        gender: "Agender",
        ip_address: "176.202.254.113",
    },
    {
        id: 6,
        first_name: "Stafford",
        last_name: "Dandy",
        email: "sdandy5@exblog.jp",
        gender: "Female",
        ip_address: "111.139.161.143",
    },
    {
        id: 7,
        first_name: "Jeramey",
        last_name: "Sweetsur",
        email: "jsweetsur6@youtube.com",
        gender: "Genderqueer",
        ip_address: "196.247.246.106",
    },
    {
        id: 8,
        first_name: "Anna-diane",
        last_name: "Wingar",
        email: "awingar7@auda.org.au",
        gender: "Agender",
        ip_address: "148.229.65.98",
    },
    {
        id: 9,
        first_name: "Cherianne",
        last_name: "Rantoul",
        email: "crantoul8@craigslist.org",
        gender: "Genderfluid",
        ip_address: "141.40.134.234",
    },
    {
        id: 10,
        first_name: "Nico",
        last_name: "Dunstall",
        email: "ndunstall9@technorati.com",
        gender: "Female",
        ip_address: "37.12.213.144",
    },
];


function allAgender(){
    return people.filter(person => person.gender === 'Agender'); 
}

function splitIPAddress(){
    return people.map(person => person.ip_address.split('.')); 
}

function sumIPComponent(ipList, index){
    return ipList.reduce((acc, curr) => acc + Number(curr[index]), 0); 
}

function filterOrgEmails(){
    return people.filter(person => person.email.endsWith('.org')); 
}

function calculateEmailDomains(domainList){
    return people.reduce((acc, curr) => {
        let domain = curr.email.split('.').slice(-1).toString();
        if(domainList.indexOf(domain) !== -1){
            if(acc[domain]){
                acc[domain] += 1; 
            }
            else{
                acc[domain] = 1; 
            } 
        }
        return acc;
    }, {});  
}

function sortDescFirstName(){
    people.sort((a,b) => {
        a = a.first_name.toLowerCase(); 
        b = b.first_name.toLowerCase(); 
        if (a > b){
            return -1; 
        }
        else if (b > a){
            return 1; 
        }
        else {
            return 0; 
        }
    });
}

// Function Calls
let aGenderPeople = allAgender(); 
console.log(aGenderPeople); 

let splitIPList = splitIPAddress(); 
console.log(splitIPList); 

let sumIPSecondComponent = sumIPComponent(splitIPList, 1); 
console.log(sumIPSecondComponent); 

let sumIPFourthComponent = sumIPComponent(splitIPList, 3); 
console.log(sumIPFourthComponent); 

let filterOrgEmailsList = filterOrgEmails(); 
console.log(filterOrgEmailsList); 

let emailDomainCount = calculateEmailDomains(['org', 'au', 'com']); 
console.log(emailDomainCount); 

sortDescFirstName(); 
console.log(people); 